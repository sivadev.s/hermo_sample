import { httpAxios } from '../../Instances/httpAxios';
import { Http } from '../../Instances/http';
import { Sample } from '../../models/sample';
const client: Http = httpAxios;
export const SampleAPI = {
    getSampleData: async () => {
        // const products = await client.get<ProductDTO>('https://api.github.com/users/mapbox');
        // return products.map((productDto: ProductDTO): Product => ());
        const samples = await client.get<any>('https://api.github.com/users/mapbox');
        return samples;
    }
};
