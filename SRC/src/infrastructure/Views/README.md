# Hermosa-Ui

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## Development server

Run `ng serve || ng serve --configuration development ` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## QA server

Run `ng serve --configuration qa` for a qa server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

# stage

Run `ng serve --configuration stage` for a stage server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

# Production server

Run `ng serve --configuration prod` for a production server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## QA -Build

Run `ng build --configuration qa` to build the project. The build artifacts will be stored in the `dist/QA/hermosa-ui` directory.

## Stage -Build

Run `ng build --configuration stage` to build the project. The build artifacts will be stored in the `dist/stage/hermosa-ui` directory.

## Production -Build

Run `ng build --configuration production` to build the project. The build artifacts will be stored in the `dist/production/hermosa-ui` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
