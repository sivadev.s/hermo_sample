import { Component } from '@angular/core';
import { Router,Event, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public activeRouter:string = '/auth'

  constructor(private router:Router){
    this.router.events.subscribe((event:Event)=>{
      if(event instanceof NavigationEnd){
        this.activeRouter = event.url;
      }
    })
  }

  get isAuthPage(){
    return (this.activeRouter.includes('/auth') || this.activeRouter === '/')
  }
}
