import { Injectable } from '@angular/core';
import { sampleDomainService } from '../../../../../domain/ports/SampleAPI';

@Injectable({
    providedIn: 'root'
})
export class SampleService {
    constructor() {}

    async getSampleData() {
        let data = await sampleDomainService.getSampleData();
        return data;
    }
}
