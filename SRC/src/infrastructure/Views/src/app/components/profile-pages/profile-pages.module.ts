import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilePagesComponent } from './profile-pages.component';
import {ProfilePagesRoutingModule} from './profile-pages.routing';


@NgModule({
    declarations: [ProfilePagesComponent,],
    imports: [CommonModule, ProfilePagesRoutingModule]
})
export class ProfilePagesModule {}
