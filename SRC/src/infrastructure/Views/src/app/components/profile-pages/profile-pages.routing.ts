import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilePagesComponent } from './profile-pages.component';

const routes: Routes = [
    {
        path: '',
        component: ProfilePagesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfilePagesRoutingModule {}
