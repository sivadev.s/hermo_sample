import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkforceComponent } from './workforce.component';
import { AddPeopleComponent } from './add-people/add-people.component';

const routes: Routes = [
    {
        path: '',
        component: WorkforceComponent
    },
    {
        path: 'add-people',
        component: AddPeopleComponent
    }

    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkforceRoutingModule {}
