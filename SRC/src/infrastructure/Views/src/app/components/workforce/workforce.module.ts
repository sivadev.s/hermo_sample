import { NgModule } from '@angular/core';
import { WorkforceRoutingModule } from './workforce.routing';
import { WorkforceComponent } from './workforce.component';
import { SharedComponentModule } from 'src/app/shared/shared.module';
import { AddPeopleComponent } from './add-people/add-people.component';

@NgModule({
    declarations: [WorkforceComponent, AddPeopleComponent],
    imports: [WorkforceRoutingModule, SharedComponentModule],
    providers: [],
    bootstrap: []
})
export class WorkforceModule {}
