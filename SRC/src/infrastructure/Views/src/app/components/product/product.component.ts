import { Component, OnInit } from '@angular/core';
import { SampleService } from 'src/app/services/sample.services';

//instances/productRepositoryFake
@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  public data:any;
    constructor(private sampleService: SampleService) {}

    ngOnInit(): void {
        this.getJSONData();
    }

    getJSONData() {
        this.sampleService
            .getSampleData()
            .then((res: any) => {
                console.log('final result', res);
                this.data=res;
            })
            .catch(err => {
                console.log('error', err);
            });
    }
}
