import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubscriberProfileComponent } from './subscriber-profile.component';

const routes: Routes = [
    {
        path: '',
        component: SubscriberProfileComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubscriberProfileRoutingModule {}
