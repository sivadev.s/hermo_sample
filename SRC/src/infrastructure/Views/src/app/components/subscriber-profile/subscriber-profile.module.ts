import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriberProfileComponent } from './subscriber-profile.component';
import { SubscriberProfileRoutingModule } from './subscriber-profile.routing';

@NgModule({
    declarations: [SubscriberProfileComponent],
    imports: [CommonModule, SubscriberProfileRoutingModule]
})
export class SubscriberProfileModule {}
