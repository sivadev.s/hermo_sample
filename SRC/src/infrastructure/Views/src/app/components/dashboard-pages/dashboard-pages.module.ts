import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-pages.routing';
import { DashboardPagesComponent } from './dashboard-pages.component';
import { SharedComponentModule } from 'src/app/shared/shared.module';
import { SuperadminPagesComponent } from './superadmin-pages/superadmin-pages.component';
import { ApprovalsComponent } from './superadmin-pages/approvals/approvals.component';
import { SubscribersComponent } from './superadmin-pages/subscribers/subscribers.component';

@NgModule({
    declarations: [DashboardPagesComponent, SuperadminPagesComponent, ApprovalsComponent, SubscribersComponent],
    imports: [DashboardRoutingModule, SharedComponentModule,CommonModule],
    providers: [],
    bootstrap: [],
    exports: [SuperadminPagesComponent]
})
export class DashboardModule {}
