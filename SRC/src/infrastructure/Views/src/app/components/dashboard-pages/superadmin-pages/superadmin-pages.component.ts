import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-superadmin-pages',
  templateUrl: './superadmin-pages.component.html',
  styleUrls: ['./superadmin-pages.component.scss']
})
export class SuperadminPagesComponent implements OnInit {
  selectedTab: string='subscribers';

  constructor() { }

  ngOnInit(): void {
  }
  tabChange(type:string){
    debugger
    this.selectedTab=type;
  }
}
