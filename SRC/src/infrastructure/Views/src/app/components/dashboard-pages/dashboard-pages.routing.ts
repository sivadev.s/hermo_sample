import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardPagesComponent } from './dashboard-pages.component';
import { ApprovalsComponent } from './superadmin-pages/approvals/approvals.component';
import { SubscribersComponent } from './superadmin-pages/subscribers/subscribers.component';
import { SuperadminPagesComponent } from './superadmin-pages/superadmin-pages.component';
const routes: Routes = [
    {
        path: '',
        component: DashboardPagesComponent
    }
    

    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {}
