import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminPagesComponent } from './superadmin-pages.component';

describe('SuperadminPagesComponent', () => {
  let component: SuperadminPagesComponent;
  let fixture: ComponentFixture<SuperadminPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperadminPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
