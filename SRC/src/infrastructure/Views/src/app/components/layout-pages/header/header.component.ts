import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input() isAuthPage: boolean | undefined;
    public header!:HTMLElement

    constructor() {}

    ngOnInit(): void {
        console.log('isAuthPage cccc', this.isAuthPage);
        this.header = document.getElementById('header')!;
        // var navcontent = document.getElementById('nav-content');
    }
}
