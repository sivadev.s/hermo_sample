import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sub-header',
    templateUrl: './sub-header.component.html',
    styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent implements OnInit {
    @Input() header!: HTMLElement;

    constructor() {}

    ngOnInit(): void {
        var navcontent = document.getElementById('nav-content')!;
        var header = this.header
        // document.addEventListener('scroll', function () {
        //     /*Refresh scroll % width*/
        //     scroll = ((h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight)) * 100;
        //     progress.style.setProperty('--scroll', scroll + '%');

        //     /*Apply classes for slide in bar*/
        //     scrollpos = window.scrollY;

        //     if (scrollpos > 10) {
        //         header.classList.add('bg-white');
        //         header.classList.add('shadow');
        //         navcontent.classList.remove('bg-gray-100');
        //         navcontent.classList.add('bg-white');
        //     } else {
        //         header.classList.remove('bg-white');
        //         header.classList.remove('shadow');
        //         navcontent.classList.remove('bg-white');
        //         navcontent.classList.add('bg-gray-100');
        //     }
        // });

        document.getElementById('nav-toggle')!.onclick = function () {
          document.getElementById('nav-content')!.classList.toggle('hidden');
        };
    }
}
