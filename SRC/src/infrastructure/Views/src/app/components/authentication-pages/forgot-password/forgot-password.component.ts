import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotForm: FormGroup = this.forgotFormBuilder.group({
    email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]})
  public submitted: boolean = false;
 

  constructor(private forgotFormBuilder: FormBuilder) {}
 

  ngOnInit(): void {
  }
  onSubmit(){
    this.submitted= true
    console.log('forgot submitted..')
    console.log(this.forgotForm.value)
  }
 
}
