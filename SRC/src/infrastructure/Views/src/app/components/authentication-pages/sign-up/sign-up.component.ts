import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
    public signUpForm: FormGroup = this.signUpFormBuilder.group({
        name: ['', Validators.required],
        organizationName: ['', Validators.required],
        numberOfEmployees: ['', Validators.required],
        mobileNumber: ['', Validators.required],
        email: ['', Validators.required]
    });

    constructor(private signUpFormBuilder: FormBuilder) {}

    ngOnInit(): void {}

    onSubmit(){
      console.log("submit..");
      console.log(this.signUpForm.value);
    }
    
}
