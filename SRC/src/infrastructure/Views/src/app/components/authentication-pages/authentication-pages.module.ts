import { NgModule } from '@angular/core';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthenticationRoutingModule } from './authentication-pages.routing';
import { SharedComponentModule } from 'src/app/shared/shared.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetComponent } from './reset/reset.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    declarations: [SignInComponent, SignUpComponent, ForgotPasswordComponent, ResetComponent],
    imports: [AuthenticationRoutingModule, SharedComponentModule, FormsModule, ReactiveFormsModule, CommonModule],
    providers: [],
    bootstrap: []
})
export class AuthenticationModule {}
