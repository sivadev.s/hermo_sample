import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  // Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/),
  public resetForm: FormGroup = this.resetFormBuilder.group({
    newpassword: new FormControl(null, [
      (c: AbstractControl) => Validators.required(c),
      Validators.pattern(
        /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
      ),
    ]),
    confirmpassword: new FormControl(null, [
      (c: AbstractControl) => Validators.required(c),
      Validators.pattern(
        /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
      ),
    ]),
  })
  public submitted: boolean = false;
  constructor(private resetFormBuilder: FormBuilder) { }

  ngOnInit(): void {
    console.log('lower', (/[0-9]/.test('STRd')))
    console.log('Cap', (/[0-9]/.test('str12d')))

  }
  onSubmit() {
    this.submitted = true
    console.log('forgot submitted..')
    console.log(this.resetForm.value)
    const newPassword = this.resetForm.get('newpassword')?.value;
    const confirmPassword = this.resetForm.get('confirmpassword')?.value;

    if (newPassword === confirmPassword) {
      console.log('Password matches',this.resetForm.value);

    } else {
      console.log('Password does not match');

    }

  }
  validatePassword(type: string, value: any) {
    if (type == 'smallletter') {
      return (/[a-z]/.test(value))
    }
    else if (type == 'capitalletter') {
      return (/[A-Z]/.test(value))
    }
    else if (type == 'number') {
      return (/[0-9]/.test(value))
    }
    else if (type == 'minlength') {
      return value.length >= 8 ? true : false;
    }
    else {
      return false;
    }
  }
}
