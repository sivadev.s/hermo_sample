import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  public signinForm: FormGroup = this.signinFormBuilder.group({
    email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    password: new FormControl(null, [
      (c: AbstractControl) => Validators.required(c),
      Validators.pattern(
        /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
      ),
    ])})
  public submitted: boolean = false;

  constructor(private signinFormBuilder: FormBuilder) { }

  ngOnInit(): void {
  }
  onSubmit(){
    this.submitted= true
    console.log('signin....')
    console.log(this.signinForm.value)
  }
}
