import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-authentication-pages',
  templateUrl: './authentication-pages.component.html',
  styleUrls: ['./authentication-pages.component.scss']
})
export class AuthenticationPagesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
