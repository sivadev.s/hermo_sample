import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import{AlertsComponent} from './alerts/alerts.component'

const SharedComponent = [AlertsComponent];

@NgModule({
    declarations: [...SharedComponent],
    imports: [CommonModule],
    providers: [],

    exports: [...SharedComponent]
})
export class SharedComponentModule {}
