import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component';
import { HeaderComponent } from './components/layout-pages/header/header.component';
import { AuthenticationPagesComponent } from './components/authentication-pages/authentication-pages.component';
import { FooterComponent } from './components/layout-pages/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubHeaderComponent } from './components/layout-pages/sub-header/sub-header.component';


@NgModule({
    declarations: [AppComponent, ProductComponent, HeaderComponent, AuthenticationPagesComponent, FooterComponent, SubHeaderComponent],
    imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
    providers: [],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule {}
