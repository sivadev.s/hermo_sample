import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./components/authentication-pages/authentication-pages.module').then(m => m.AuthenticationModule)
    },
    {
        path: 'profile',
        loadChildren: () => import('./components/profile-pages/profile-pages.module').then(m => m.ProfilePagesModule)
    },
    {
        path: 'subscriber',
        loadChildren: () => import('./components/subscriber-profile/subscriber-profile.module').then(m => m.SubscriberProfileModule)
    },
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        loadChildren: () => import('./components/dashboard-pages/dashboard-pages.module').then(m => m.DashboardModule)
    },
    {
        path: 'workforce',
        loadChildren: () => import('./components/workforce/workforce.module').then(m => m.WorkforceModule)
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
